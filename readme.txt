新的开始重新学习Git命令，放弃SourceTree的使用
1、测试初始化一个空的仓库，并且添加一个文件用来描述主分支的功能。

2、Git主分支的名字默认叫做Master，版本库初始化以后，自动创建并且默认在主分支上面工作,主分支是提供给用户使用的正式版本，需要发布的版本都在这个分支上面发布。

3、发布的版本使用如下命令来创建一个标签: git tag 1.0.0 1b2afadf1e, 后面的十位是用来标记提交的ID，git log来获取提交的ID。
可以添加注释，例如git tag -a v0.1.1 your_commit_sha1  -m '发布版本0.1.1'
    解释: -a表明是annoted类型的tag，可以保留一些相关信息，不加说明是lightweight类型的tag。
在tag还没有使用git push之前，我们可以使用git tag -d your_version_tagname来删除它。
可以使用git push origin your_version_tagname来push这个tag到远程分支，也可以通过git push origin --tags  一次性的将所有本地新增的tag推送到远程分支，这样别人clone仓库才可以看到分支标志。


4、深入理解撤销几个命令的使用
git checkout -- file_name 用户撤销文件的修改，包括两种情况
一是修改后未加入暂存，那么撤销修改后和版本库的内容一致。
二是已经加入暂存区后再修改，那么撤销修改操作后的内容和暂存区的内容一致。
