-- 阅读Git - Book
熟练Git命令行

第二章：Git基础

1、取出项目的Git仓库
有两种方法获取Git项目仓库的方法，第一种是在现存的目录下，通过导入所有文件夹来创建新的Git仓库
，第二种是从已有的Git仓库克隆出一个新的镜像仓库来。

方法一：进入需要用Git管理的目录，通过命令行:
$git init
初始化，会在当前目录生成一个.git的目录，所有Git需要的数据和资源都放在这个目录中，这个时候还
没有追踪项目中的任何文件，仅仅是生成了既有的结构的git目录，通过命令行来追踪并提交：
$git add *.c
$git add README
$git commit -m 'initial project version' 
上面的几条命令可以简写的方式(自动将所有的文件添加追踪，假如暂存区，然后提交):
$git commit -a -m 'comment'

方法二：从现有的仓库克隆
克隆仓库的命令格式为：git clone [url],比如，要克隆Ruby语言的Git代码仓库Grit，可以使用：
$git clone git://github.com/schacon/grit.git
这会在当前目录下创建一个名为grit的目录，其中包含一个.git的目录，用于保存下载下载的所有版本
记录，然后从中取出最新版本的文件拷贝，如果希望自定义新建的项目目录的名称，可以使用在上面的命
令末尾制定新的名字。git支持需要数据传输协议，比如上面的git://协议，也可以使用https://或者
user@server:/path.git表示的SSH传输协议。

2、记录每次更新到仓库
记住：工作目录下面的所有文件不外乎两种状态，已跟踪和未跟踪。
已跟踪：指已经被纳入版本控制器管理的文件，上一次快照有他们的记录，工作一段时间后，它们的状态
	   可能是未修改、已修改或者已暂存。
未跟踪：未添加如版本管理的，没有上次更新的快照，也不在当前的暂存区域。
初次克隆某个仓库后，工作目录中的所有文件都属于已跟踪文件，并且状态为未修改，在编辑过某些文件
后，Git将这些文件标记为已修改，我们逐步将修改过的文件放入暂存区域，直到最后一次性提交所有这
些暂存起来的文件，如此重复，状态转换图如下:
untracked	unmodified	modified	staged
    |	    	|	edit-->	|			|
    |	add-- >	|			|			|
    |< --remove	|			|  stage-->	|
    |			|	< -- commit			|

3、检查文件的状态
$git status

4、跟踪新文件
$git add filename
注：其实git add的潜台词是把目标文件快照放入暂存区域，也就是add file into staged area，
同时未曾跟踪过的文件标记为需要跟踪。

5、暂存已修改的文件
$git add filename

例：如果一个已经追踪的文件A，修改后加入暂存区域，这个时候，你再次进行修改，那么你用
git status查看会发现文件出现了两次，一次算未暂存，一次算已暂存，如果现在提交，那么
提交的是已暂存的版本，而非当前目录中的版本，如果需要把本地的也提交，那么需要使用git 
add先暂存，然后提交。

6、忽略某些文件
一般我们总一些文件不需要纳入git管理，也不希望出现在未跟踪的文件列表。通常都是些自动
生成的文件，比如日志文件，或者编译的临时文件，我们可以创建一个.gitignore的文件，列出
要忽略的文件模式。

7、查看已暂存和未暂存的更新
git status显示的比较简单，仅仅是列出修改过的文件，如果要查看修改了什么地方，可以使用命令：
$git diff 
注：使用文件补丁的形式显示添加和删除的行，此命令比较的是工作目录中的文件和暂存区域的快照之间
的差异，也就是修改之后还没有暂存起来的变化。如果要看已经暂存起来的文件和上次提交的快照之间的
差异可以使用命令：
$git diff --cache #高版本使用git diff --staged

8、提交更新
$git commit -m "conment"

9、跳过暂存区域
$git commit -a -m 'conment'
尽管暂存区域的方式可以精心准备要提交的细节，但有时候过于繁琐，Git提供了一个跳过暂存区域的方
式，只要在提交的时候加上-a选项，git会自动把所有已经跟踪过的文件暂存起来一并提交，从而跳过
加入暂存的步骤。

10、移除文件
$git rm filename [-f] [--cache]
要从Git中移除某个文件，那必须要从已跟踪文件清单中移除(确切的说，是从暂存区移除)，然后提交，
可以使用git rm命令完成此项工作，并连带从工作目录中删除指定的文件，这样以后就不会出现在未跟
踪文件清单中了。如果只是简单的从工作目录中删除文件，那么运行git status会提示"Changes not
staged for commit"部分。如果移除文件之前已经加入暂存区域，那么需要使用-f参数，如果移除不想
完全删除，可以通过--cache参数来控制，会转变为未跟踪的状态。

11、移动文件
$git mv file_from file_to

12、查看历史
$git log

13、修改最后一次提交
$git commit --amend
如果上一次提交时忘记暂存某一些需要修改的文件，可以先补上暂存操作，然后再运行--amend提交：
$git commit -m "commit"
$git add forgotten_file
$git commit --amend
注：上面的三条命令最终只是产生一个提交，第二个提交命令修正了第一个的提交内容。

14、取消已经暂存的文件
$git reset file_name

15、取消对文件的修改
$git checkout -- file_name
这条命令有些危险，直接把所有的修改都覆盖了，如果只是想回退版本，同时保留刚才的修改以便继续
工作，可以用stashing和分支来处理。

注：任何已经提交到Git的都可以被恢复，即便已经删除的分支中的提交，或者用--amend重新改写的
提交，都可以被恢复，所以，你可能丢失的数据仅限于没有提交过的，对Git来说他们就像不存在一样。

16、远程仓库的使用
查看当前远程仓库：
$git remote -v

添加远程仓库：
$git remote add [remote-name] [url]

从远程仓库抓取数据：
$git fetch [remote-name]

从远程抓取数据并合并到本仓库的当前分支：
$git pull [remote-name]

推送数据到远程仓库
$git push [remote-name] [branch-name]

查看远程仓库信息:
$git remote show [remote-name]

远程仓库的删除和重命名:
$git remote rename old_name new_name

17、打标签
列出已有的标签：
$git tag
注：我们可以用列出符合条件的标签，例如只1.4.2系列版本，git tag -l 'v1.4.2.*'

新建标签：
Git使用的标签有两种类型：轻量级的(lightweight)和含附注的(annotated)。含附注的标签命令：
$git tag -a v1.4.1 -m '1.4.1版本正式版发布'
轻量级的标签直接git tag name,不需要参数选项。

分享标签：
$git push [remote-name] [tag-name]
注：默认情况下git push不会推送标签，可以添加配置选项--tags来一次性添加修改和标签。

18、Git技巧和敲门
自动补全
下载Git源码，进入contrib/completion目录，看到一个git-completion.bash文件，将此文件复制
到你自己的用户主目录中，然后执行下面命令即可：
$cp git-completion.bash ~/.git-completion.bash
$source ~/.git-completion.bash
也可以为系统上所有用户都设置默认使用此脚本，Mac上将此脚本复制到目录
/opt/local/etc/bash_completion.d,这个目录在bash启动时自动加载。
在敲命令的时候双击Tab即可列出所有匹配的命令。

Git命令别名
git config --global alias.co checkout
git config --global alias.br branch
git config --global alias.ci commit
git config --global alias.st status

$git config --global alias.unstage 'reset HEAD --'
下面的两条命令完全等同：
$git unstage fileA
$git reset HEAD fileA

$git config --global alias.last 'log -l HEAD'

配置颜色
git config --global color.status auto
git config --global color.branch auto
git config --global color.diff auto
git config --global color.grep auto
git config --global color.interactive auto
git config --global color.ui auto




















